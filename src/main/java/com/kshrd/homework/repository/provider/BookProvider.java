package com.kshrd.homework.repository.provider;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {


    public String insertBookSql() {
        return new SQL(){{
            INSERT_INTO("tb_books");
            VALUES("title", "#{title}");
            VALUES("author", "#{author}");
            VALUES("description", "#{description}");
            VALUES("thumbnail", "#{thumbnail}");
            VALUES("category_id", "#{category_id}");
        }}.toString();
    }

    public String selectBookSql() {
        return new SQL(){{
            SELECT("b.id, b.title, b.author, b.description, c.id as cid, c.title as ctitle");
            FROM("tb_books b");
            INNER_JOIN("tb_categories c ON b.category_id = c.id");
        }}.toString();
    }

    public String selectBookByIDSql(@Param("id") int id) {
        return new SQL(){{
            SELECT("b.id, b.title, b.author, b.description, c.id as cid, c.title as ctitle");
            FROM("tb_books b");
            INNER_JOIN("tb_categories c ON b.category_id = c.id");
            WHERE("b.id=#{id}");
        }}.toString();
    }

    public String deleteBookSql(@Param("id") int id){
        return new SQL(){{
            DELETE_FROM("tb_books");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String updateBookSql(@Param("id") int id){
        return new SQL(){{
            UPDATE("tb_books");
            SET("title=#{bookDto.title}, author=#{bookDto.author}, description=#{bookDto.description}, category_id=#{bookDto.category_id}");
            WHERE("id=#{id}");
        }}.toString();
    }
}
