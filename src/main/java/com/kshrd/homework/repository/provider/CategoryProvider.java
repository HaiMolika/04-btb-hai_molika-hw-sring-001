package com.kshrd.homework.repository.provider;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {

    public String insertCategorySql() {
        return new SQL(){{
            INSERT_INTO("tb_categories");
            VALUES("title", "#{title}");
        }}.toString();
    }

    public String selectCategorySql() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_categories");
        }}.toString();
    }

    public String selectCategoryByIDSql(@Param("id") int id){
        return new SQL(){{
            SELECT("*");
            FROM("tb_categories");
            WHERE("id=#{id}");
        }}.toString();
    }

    public String updateCategory(@Param("id") int id){
        return new SQL(){{
           UPDATE("tb_categories");
           SET("title = #{categoryDto.title}");
           WHERE("id=#{id}");
        }}.toString();
    }

    public String deleteCategory(@Param("id") int id){
        return new SQL(){{
            DELETE_FROM("tb_categories");
            WHERE("id=#{id}");
        }}.toString();
    }

}
