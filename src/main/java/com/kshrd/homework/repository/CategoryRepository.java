package com.kshrd.homework.repository;

import com.kshrd.homework.repository.dto.CategoryDto;
import com.kshrd.homework.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.DeleteProvider;
import org.apache.ibatis.annotations.InsertProvider;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.UpdateProvider;
import org.postgresql.util.PSQLException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @InsertProvider(type = CategoryProvider.class, method = "insertCategorySql")
    boolean insert(CategoryDto categoryDto) throws PSQLException;

    @SelectProvider(type = CategoryProvider.class, method = "selectCategorySql")
    List<CategoryDto> select ();

    @SelectProvider(type = CategoryProvider.class, method = "selectCategoryByIDSql")
    CategoryDto selectByID (int id);

    @DeleteProvider(type = CategoryProvider.class, method = "deleteCategory")
    boolean delete (int id);

    @UpdateProvider(type = CategoryProvider.class, method = "updateCategory")
    boolean update (int id, CategoryDto categoryDto) throws PSQLException;
}
