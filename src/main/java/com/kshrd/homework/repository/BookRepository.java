package com.kshrd.homework.repository;

import com.kshrd.homework.repository.dto.BookDto;
import com.kshrd.homework.repository.provider.BookProvider;
import com.kshrd.homework.rest.response.BookResponse;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface BookRepository {

    @InsertProvider(type = BookProvider.class, method="insertBookSql" )
    boolean insert (BookDto bookDto);

    @SelectProvider(type = BookProvider.class, method="selectBookSql")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author"),
            @Result(property = "description", column = "description"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "categoryDto.id", column = "cid"),
            @Result(property = "categoryDto.title", column = "ctitle")
    })
    List<BookResponse> select();

    @SelectProvider(type = BookProvider.class, method = "selectBookByIDSql")
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "title", column = "title"),
            @Result(property = "author", column = "author"),
            @Result(property = "description", column = "description"),
            @Result(property = "thumbnail", column = "thumbnail"),
            @Result(property = "categoryDto.id", column = "cid"),
            @Result(property = "categoryDto.title", column = "ctitle")
    })
    BookResponse selectByID (int id);

    @DeleteProvider(type = BookProvider.class, method = "deleteBookSql")
    void delete (int id);

    @UpdateProvider(type = BookProvider.class, method = "updateBookSql")
    boolean update (int id, BookDto bookDto);
}
