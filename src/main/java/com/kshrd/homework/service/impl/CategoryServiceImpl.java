package com.kshrd.homework.service.impl;

import com.kshrd.homework.repository.dto.CategoryDto;
import com.kshrd.homework.repository.CategoryRepository;
import com.kshrd.homework.service.CategoryService;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {

    private CategoryRepository categoryRepository;

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository){
        this.categoryRepository = categoryRepository;
    }

    @Override
    public CategoryDto insert(CategoryDto categoryDto) {
        boolean isInserted = false;
        try {
            isInserted = categoryRepository.insert(categoryDto);
        } catch (PSQLException throwables) {
            throwables.printStackTrace();
        }
        if(isInserted)
            return categoryDto;
        else
            return null;
    }

    @Override
    public List<CategoryDto> select() {
        return categoryRepository.select();
    }

    @Override
    public CategoryDto selectByID(int id) {
        return categoryRepository.selectByID(id);
    }

    @Override
    public void delete(int id) {
        categoryRepository.delete(id);
    }

    @Override
    public CategoryDto update(int id, CategoryDto categoryDto) {
        boolean isUpdated = false;
        try {
            isUpdated = categoryRepository.update(id, categoryDto);
        } catch (PSQLException throwables) {
            throwables.printStackTrace();
        }
        if(isUpdated)
            return categoryDto;
        else
            return null;
    }
}