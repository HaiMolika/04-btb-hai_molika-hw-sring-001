package com.kshrd.homework.service.impl;

import com.kshrd.homework.repository.BookRepository;
import com.kshrd.homework.repository.dto.BookDto;
import com.kshrd.homework.rest.response.BookResponse;
import com.kshrd.homework.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookDto insert(BookDto bookDto) {
        boolean isInserted = bookRepository.insert(bookDto);
        if(isInserted)
            return bookDto;
        else
            return null;
    }

    @Override
    public List<BookResponse> select() {
        return bookRepository.select();
    }

    @Override
    public BookResponse selectByID(int id) {
        return bookRepository.selectByID(id);
    }

    @Override
    public BookDto update(int id, BookDto bookDto) {

        boolean isUpdated = bookRepository.update(id, bookDto);
        if(isUpdated)
            return bookDto;
        else
            return null;
    }

    @Override
    public void delete(int id) {
        bookRepository.delete(id);
    }
}
