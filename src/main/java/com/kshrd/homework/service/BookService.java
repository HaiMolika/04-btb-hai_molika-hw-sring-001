package com.kshrd.homework.service;

import com.kshrd.homework.repository.dto.BookDto;
import com.kshrd.homework.rest.response.BookResponse;

import java.util.List;

public interface BookService {
    BookDto insert(BookDto bookDto);
    List<BookResponse> select();
    BookResponse selectByID(int id);
    BookDto update(int id, BookDto bookDto);
    void delete(int id);
}
