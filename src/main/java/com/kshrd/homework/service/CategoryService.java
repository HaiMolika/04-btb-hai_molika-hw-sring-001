package com.kshrd.homework.service;

import com.kshrd.homework.repository.dto.CategoryDto;

import java.util.List;

public interface CategoryService {

    CategoryDto insert (CategoryDto categoryDto);
    List<CategoryDto> select();
    CategoryDto selectByID (int id);
    void delete (int id);
    CategoryDto update (int id, CategoryDto categoryDto);

}
