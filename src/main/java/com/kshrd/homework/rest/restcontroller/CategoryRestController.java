package com.kshrd.homework.rest.restcontroller;

import com.kshrd.homework.repository.dto.CategoryDto;
import com.kshrd.homework.rest.request.CategoryRequestModel;
import com.kshrd.homework.rest.response.ApiResponse;
import com.kshrd.homework.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CategoryRestController {

    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping("/categories")
    public ResponseEntity<ApiResponse<CategoryRequestModel>> insert (
            @RequestBody CategoryRequestModel categoryRequestModel
    ){
        ApiResponse<CategoryRequestModel> response = new ApiResponse<>();

        CategoryDto categoryDto = new ModelMapper().map(categoryRequestModel, CategoryDto.class);

        CategoryDto insertedCategory = categoryService.insert(categoryDto);

        CategoryRequestModel result = new ModelMapper().map(insertedCategory, CategoryRequestModel.class);

        response.setMessage("Okay");
        response.setData(result);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/categories")
    public ResponseEntity<ApiResponse<List<CategoryDto>>> select(){

        ApiResponse<List<CategoryDto>> response = new ApiResponse<>();
        List<CategoryDto> categoryDto = categoryService.select();

        response.setMessage("Okay");
        response.setStatus(HttpStatus.OK);
        response.setData(categoryDto);

        return ResponseEntity.ok(response);

    }

    @GetMapping("/categories/{id}")
    public ResponseEntity<ApiResponse<CategoryDto>> selectByID(@PathVariable int id) {

        ApiResponse<CategoryDto> response = new ApiResponse<>();
        CategoryDto categoryDto = categoryService.selectByID(id);

        response.setMessage("Found");
        response.setStatus(HttpStatus.OK);
        response.setData(categoryDto);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/categories/{id}")
    public ResponseEntity<String> delete(@PathVariable int id){
        categoryService.delete(id);
        return ResponseEntity.ok("Successfully delete");
    }

    @PutMapping("/categories/{id}")
    public ResponseEntity<ApiResponse<CategoryRequestModel>> update (
            @PathVariable int id,
            @RequestBody CategoryRequestModel categoryRequestModel
    ){
        ApiResponse<CategoryRequestModel> response = new ApiResponse<>();

        CategoryDto categoryDto = new ModelMapper().map(categoryRequestModel, CategoryDto.class);

        CategoryDto updatedCategory = categoryService.update(id, categoryDto);

        CategoryRequestModel result = new ModelMapper().map(updatedCategory, CategoryRequestModel.class);

        response.setMessage("Successfully updated");
        response.setStatus(HttpStatus.OK);
        response.setData(result);


        return ResponseEntity.ok(response);
    }

}
