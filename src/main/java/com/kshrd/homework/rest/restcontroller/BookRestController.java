package com.kshrd.homework.rest.restcontroller;

import com.kshrd.homework.repository.dto.BookDto;
import com.kshrd.homework.rest.request.BookRequestModel;
import com.kshrd.homework.rest.response.ApiResponse;
import com.kshrd.homework.rest.response.BookResponse;
import com.kshrd.homework.service.impl.BookServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookRestController {

    private BookServiceImpl bookService;

    @Autowired
    public void setBookService(BookServiceImpl bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/books")
    public ResponseEntity<ApiResponse<BookRequestModel>> insert(
        @RequestBody BookRequestModel bookRequestModel
    ){

        ApiResponse<BookRequestModel> response = new ApiResponse<>();

        BookDto bookDto = new ModelMapper().map(bookRequestModel, BookDto.class);

        BookDto insertedBook = bookService.insert(bookDto);

        BookRequestModel result = new ModelMapper().map(insertedBook, BookRequestModel.class);

        response.setMessage("Okay");
        response.setStatus(HttpStatus.OK);
        response.setData(result);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/books")
    public ResponseEntity<ApiResponse<List<BookResponse>>> select (){

        ApiResponse<List<BookResponse>> response = new ApiResponse<>();
        List<BookResponse> bookDtoList = bookService.select();

        response.setMessage("Okay");
        response.setStatus(HttpStatus.OK);
        response.setData(bookDtoList);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<ApiResponse<BookResponse>> selectByID(@PathVariable int id){

        ApiResponse<BookResponse> response = new ApiResponse<>();
        BookResponse bookResponse = bookService.selectByID(id);

        response.setMessage("Okay");
        response.setStatus(HttpStatus.OK);
        response.setData(bookResponse);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/books/{id}")
    public ResponseEntity<String> delete(@PathVariable int id){
        bookService.delete(id);
        return ResponseEntity.ok("Successfully deleted");
    }

    @PutMapping("/books/{id}")
    public ResponseEntity<ApiResponse<BookRequestModel>> update (
            @PathVariable int id,
            @RequestBody BookRequestModel bookRequestModel
    ){
        ApiResponse<BookRequestModel> response = new ApiResponse<>();
        BookDto bookDto = new ModelMapper().map(bookRequestModel, BookDto.class);

        BookDto insertedBook = bookService.update(id, bookDto);
        BookRequestModel result = new ModelMapper().map(insertedBook, BookRequestModel.class);

        response.setMessage("Okay");
        response.setStatus(HttpStatus.OK);
        response.setData(result);

        return ResponseEntity.ok(response);
    }
}
